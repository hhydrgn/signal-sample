#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)

int main()
{
    debug_log("entered %s\n", __FILE__);

    int ret = EXIT_FAILURE;
    pid_t pid = fork();

    if (pid < 0) {
        perror("fork()");
        goto end;
    }

    if (pid == 0) {
        debug_log("start child process\n");
        sleep(3);
        if (kill(getppid(), SIGCONT) < 0) {
            perror("SIGCONT");
            goto end;
        }
        printf("\n");
        debug_log("finish child process\n");
    }

    if (pid && kill(getpid(), SIGSTOP) < 0) {
        perror("SIGSTOP");
        goto end;
    }
    ret = EXIT_SUCCESS;

end:
    debug_log("exit %s return %d\n", __FILE__, ret);
    return ret;
}
