#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)

static unsigned int sigint_count = 0;
static unsigned int sigtstp_count = 0;
static unsigned int sigquit_count = 0;

void signal_handler_test(int no)
{
    switch(no) {
    case SIGINT:
        ++sigint_count;
        break;
    case SIGTSTP:
        ++sigtstp_count;
        break;
    case SIGQUIT:
        ++sigquit_count;
        break;
    }
}

int main()
{
    debug_log("entered %s\n", __FILE__);

    sigset_t block, oblock;
    struct sigaction sa;

    sigemptyset(&block);
    sigemptyset(&oblock);
    /* sa_handler 実行中に禁止するシグナル */
    sigaddset(&block, SIGINT);
    sigaddset(&block, SIGTSTP);
    sigaddset(&block, SIGQUIT);

    sa.sa_handler = signal_handler_test;
    sa.sa_flags = SA_RESTART;
    sa.sa_mask = block;
    /* sa_handler 実行のトリガー */
    sigaction(SIGINT, &sa, 0);
    sigaction(SIGTSTP, &sa, 0);
    sigaction(SIGQUIT, &sa, 0);

    debug_log("press Ctrl-C(INT), Ctrl-Z(TSTP) or Ctrl-\\(QUIT)\n");
    while(!sigquit_count) {
        sigprocmask(SIG_BLOCK, &block, &oblock); /* disable */
        debug_log("sigint count: %u, ", sigint_count);
        printf("sigtstp count: %u\n", sigtstp_count);
        sigprocmask(SIG_SETMASK, &oblock, NULL); /* enable */
        pause(); /* wait signal */
    }

    debug_log("exit %s\n", __FILE__);
    return 0;
}
