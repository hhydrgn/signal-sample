targets = sigstop_sigcont sa_mask_ex

all: $(targets)

%: %.c
	gcc -g -o $@ $<

clean:
	rm -f $(targets)
