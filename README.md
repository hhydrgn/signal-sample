**coding style**

http://httpd.apache.org/dev/styleguide.html

    $ indent -i4 -npsl -di0 -br -nce -d0 -cli0 -npcs -nfc1 -nut
    $ cppcheck --enable=all

**usage**

	$ make
	$ ./sigstop_sigcont
    $ ./sa_mask_ex

**reference**

[SA_RESTART](http://ja.stackoverflow.com/questions/5469/linux-application-signal-sa-restartの使いどころ)

[catch signal](https://sourceware.org/gdb/onlinedocs/gdb/Set-Catchpoints.html#Set-Catchpoints)

**send signal**

 * Ctrl-C : SIGINT; terminate
 * Ctrl-Z : SIGTSTP; suspend
 * Ctrl-\ : SIGQUIT; terminate and dump core

**signal**

 1. HUP
 1. INT
 1. QUIT
 1. ILL
 1. TRAP
 1. ABRT
 1. BUS
 1. FPE
 1. KILL
 1. USR1
 1. SEGV
 1. USR2
 1. PIPE
 1. ALRM
 1. TERM
 1. STKFLT
 1. CHLD
 1. CONT
 1. STOP
 1. TSTP
 1. TTIN
 1. TTOU
 1. URG
 1. XCPU
 1. XFSZ
 1. VTALRM
 1. PROF
 1. WINCH
 1. POLL
 1. PWR
 1. SYS
